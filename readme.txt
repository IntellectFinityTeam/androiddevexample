Bibbucket 101 could be found at https://confluence.atlassian.com/display/BITBUCKET/Bitbucket+101
Please check below topics before continue
- Set up Git
- Create a Bitbucket account 

----------------------------------------------------

How to sync the project:
- Crate a folder 'BitBucket'
- (Optional) Set Git Init Here at folder 'BitBucket'
- Open GitBash and broser to folder 'BitBucket'
- Run "git clone https://IntellectFinity@bitbucket.org/IntellectFinityTeam/androiddevexample.git", to clone 'AndroidDevExample' project under 'BitBucker' folder


----------------------------------------------------

How to import the project to Android Studio:
- File Menu -> New -> Import Project
- Browse to the 'AndroidDevExample' folder -> Click Ok
- Build Menu -> Rebuild Project
- Now you are ready to go. ^^

----------------------------------------------------

If you have any question, please feel free to contact IntellectFinity@gmail.com