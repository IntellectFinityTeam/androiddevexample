package intellectfinity.app.launcher;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Launcher extends ListActivity {
    String[] itemNames;
    String[] classNames;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        itemNames = getResources().getStringArray(R.array.item_names);
        classNames = getResources().getStringArray(R.array.class_names);
        setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, itemNames));
    }

    @Override
    protected void onListItemClick(ListView list, View view, int position, long id) {
        super.onListItemClick(list, view, position, id);
        String className = classNames[position];
        try {
            Class clazz = Class.forName("intellectfinity.lib.example." + className);
            Intent intent = new Intent(this, clazz);
            startActivity(intent);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}