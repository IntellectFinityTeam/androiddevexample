package intellectfinity.lib.example;

// This class is a data object which will be set and added to dataAdapter for list view
public class A102I_ListViewAdvDataProvider {
    private int emoImageResource;
    private String emoDescription;

    public A102I_ListViewAdvDataProvider(
            int emoImageResource,
            String emoDescription) {
        this.emoImageResource = emoImageResource;
        this.emoDescription = emoDescription;
    }

    public int getEmoImageResource() {
        return emoImageResource;
    }

    public void setEmoImageResource(int emoImageResource) {
        this.emoImageResource = emoImageResource;
    }

    public String getEmoDescription() {
        return emoDescription;
    }

    public void setEmoDescription(String emoDescription) {
        this.emoDescription = emoDescription;
    }
}