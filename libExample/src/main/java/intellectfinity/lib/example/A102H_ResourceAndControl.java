package intellectfinity.lib.example;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;

public class A102H_ResourceAndControl extends AppCompatActivity {

    ArrayList<String> checkedItems = new ArrayList<String>();

    Spinner monthSpinner;
    Spinner daySpinner;
    int monthSpinnerPosition;
    int daySpinnerPosition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set created layout/view to this class.
        setContentView(R.layout.a102h_resource_and_control);

        // Toggle Switch.........................................
        final Switch switchButton = (Switch) findViewById(R.id.toggle_sound);
        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            // This method will be triggered by a toggle switch event listener
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // Verify toggle button status, get image resource and create text message
                String textMessage = "";
                String uri = "";
                if (isChecked) {
                    uri = "@drawable/ic_action_volume_up";
                    textMessage = "Toggle Button - Sound on";
                } else {
                    uri = "@drawable/ic_action_volume_mute";
                    textMessage = "Toggle Button - Sound off";
                }
                int imageResource = getResources().getIdentifier(uri, null, getPackageName());

                // Get/Set image view
                ImageView imageView = (ImageView) findViewById(R.id.image_sound);
                imageView.setVisibility(View.VISIBLE);
                imageView.setImageResource(imageResource);

                // Get/Set text view
                TextView text_toggle = (TextView) findViewById(R.id.text_toggle);
                text_toggle.setText(textMessage);
            }
        });

        // Spinner...............................................
        monthSpinner = (Spinner) findViewById(R.id.spinner_month);
        monthSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            // This method will be triggered by a spinner event listener
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                monthSpinnerPosition = position;

                // A method to create and update the spinner text
                updateSpinnerText();
            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing, just another required interface callback
            }
        });

        // The daySpinner has not load spinner item yet.
        // Get string array from resource then create array adapter for spinner
        daySpinner = (Spinner) findViewById(R.id.spinner_day);
        ArrayAdapter<CharSequence> adapterSpinner = ArrayAdapter.createFromResource(
                this,
                R.array.day_arrays,
                android.R.layout.simple_spinner_item);

        // Set all items as dropdown list
        adapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        daySpinner.setAdapter(adapterSpinner);

        daySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            // This method will be triggered by a spinner event listener
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                daySpinnerPosition = position;

                // A method to create and update the spinner text
                updateSpinnerText();
            }

            public void onNothingSelected(AdapterView<?> parent) {
                // Do nothing, just another required interface callback
            }
        });

        // Set home should be displayed as an "up" affordance on ActionBar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    // This method will be called by spinner event listener
    // To create text message and update the spinner text
    private void updateSpinnerText() {
        // Create text message
        String textMessage =
                "Spinner - Month=" + monthSpinner.getItemAtPosition(monthSpinnerPosition) +
                        ", Day=" + daySpinner.getItemAtPosition(daySpinnerPosition) +
                        " is selected";

        // Get/Set text view
        TextView text_spinner = (TextView) findViewById(R.id.text_spinner);
        text_spinner.setText(textMessage);
    }

    // Button............................................
    // This method will be triggered by a button event listener
    public void clickButton(View view) {
        String textMessage = "";

        // Verify clicked button and create text message
        int i = view.getId();
        if (i == R.id.button_label) {
            textMessage = "Button label is clicked.";
        } else if (i == R.id.button_image) {
            textMessage += "Button image is clicked.";
        } else if (i == R.id.button_mixed) {
            textMessage += "Button mixed is clicked.";
        }

        // Get/Set text view
        TextView text_button = (TextView) findViewById(R.id.text_button);
        text_button.setText(textMessage);
    }

    // Checkbox..........................................
    // This method will be triggered by a checkbox event listener
    public void checkItem(View view) {
        // Verify checked item and update checked items array list
        String company = ((CheckBox) view).getText().toString();
        boolean checked = ((CheckBox) view).isChecked();
        if (checked) {
            checkedItems.add(company);
        } else {
            checkedItems.remove(company);
        }

        // Create text message from checked item array list
        String textMessage = "Checkbox - ";
        for (String checkItem : checkedItems) {
            textMessage += checkItem + " ";
        }

        // Get/Set text view
        TextView text_checkbox = (TextView) findViewById(R.id.text_checkbox);
        text_checkbox.setText(textMessage);
    }

    // Radio Button......................................
    // This method will be triggered by a radio button event listener
    public void selectRadio(View view) {
        // Verify selected radio and create text message
        String textMessage = "Radio Button - " + ((RadioButton) view).getText().toString() + " is selected.";

        // Get/Set text view
        TextView text_radio = (TextView) findViewById(R.id.text_radio);
        text_radio.setText(textMessage);
    }

    // Toggle Button.....................................
    // This method will be triggered by a toggle button event listener
    public void toggleButton(View view) {
        // Verify toggle button status and create text message
        boolean checked = ((ToggleButton) view).isChecked();
        String textMessage = "";
        if (checked) {
            textMessage = "Toggle Button - WiFi on";
        } else {
            textMessage = "Toggle Button - WiFi off";
        }

        // Get/Set text view
        TextView text_toggle = (TextView) findViewById(R.id.text_toggle);
        text_toggle.setText(textMessage);

        // Hide the image view since it will be used with toggle switch
        ImageView imageView = (ImageView) findViewById(R.id.image_sound);
        imageView.setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.example_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Close the current activity
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
