package intellectfinity.lib.example;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

public class A102K_Fragment extends AppCompatActivity implements
        A102K_FragmentSender.OnColorChangedListener,
        A102K_FragmentSender.OnButtonClickedListener {

    LinearLayout mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set created layout/view to this class.
        setContentView(R.layout.a102k_fragment);

        // Dynamic Fragment
        final A102K_FragmentA firstFragment = new A102K_FragmentA();
        final A102K_FragmentB secondFragment = new A102K_FragmentB();

        // Set up text view with event listener for replacing a fragment A.
        TextView firstTextView = (TextView) findViewById(R.id.text_fragment_a);
        firstTextView.setTextColor(Color.BLUE);
        firstTextView.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        firstTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            // This will be triggered by a text view event listener
            // Use fragmentManager, fragmentTransaction to replace a fragment.
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction =
                        fragmentManager.beginTransaction();
                fragmentTransaction.replace(
                        R.id.fragment_dynamic,
                        firstFragment);
                fragmentTransaction.commit();
            }
        });

        // Set up text view with event listener for replacing a fragment B.
        TextView secondTextView = (TextView) findViewById(R.id.text_fragment_b);
        secondTextView.setTextColor(Color.BLUE);
        secondTextView.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);
        secondTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            // This will be triggered by a text view event listener
            // Use fragmentManager, fragmentTransaction to replace a fragment.
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction =
                        fragmentManager.beginTransaction();
                fragmentTransaction.replace(
                        R.id.fragment_dynamic,
                        secondFragment);
                fragmentTransaction.commit();
            }
        });

        // For Fragment to Main Activity
        // Find the linear layout object that will be updated
        // when sender fragment passing message/data to main activity.
        mainLayout = (LinearLayout) findViewById(R.id.fragment_main);
    }

    // For Fragment to Main Activity .......................................
    // This is an implementation of sender fragment interface.
    // Fragment use this interface to communicate between fragment and activity
    // Please check sender fragment onAttach and onCreateView for more info
    @Override
    public void colorChanged(String colorName) {
        switch (colorName) {
            case "Red":
                mainLayout.setBackgroundColor(getResources().
                        getColor(android.R.color.holo_red_light));
                break;
            case "Green":
                mainLayout.setBackgroundColor(getResources().
                        getColor(android.R.color.holo_green_light));
                break;
            case "Blue":
                mainLayout.setBackgroundColor(getResources().
                        getColor(android.R.color.holo_blue_light));
                break;
            case "Gray":
                mainLayout.setBackgroundColor(Color.parseColor("#EEEEEE"));
                break;
        }
    }

    // For Fragment to another Fragment .......................................
    // This is an implementation of fragment interface.
    // Fragment use this interface to communicate between fragment and activity
    // Please check sender fragment onAttach and onCreateView for more info
    @Override
    public void buttonClicked(String colorName) {
        // - Sender fragment call this method with input parameter
        // - Instantiate receiver fragment and call the object method with input parameter.
        A102K_FragmentReceiver receiverFragment =
                (A102K_FragmentReceiver) getFragmentManager().
                        findFragmentById(R.id.fragment_receiver);
        receiverFragment.updateBackgroundColor(colorName);
    }

}

