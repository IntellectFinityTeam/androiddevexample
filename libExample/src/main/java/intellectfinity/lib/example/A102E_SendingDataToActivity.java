package intellectfinity.lib.example;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class A102E_SendingDataToActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set created layout to this class.
        setContentView(R.layout.a102e_sending_data_to_activity);

        // Set home should be displayed as an "up" affordance on ActionBar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    // This method will be triggered by an event listener
    public void sendToActivity(View view) {
        // Find the edit text object and get the text message.
        EditText editTextMessage = (EditText) findViewById(R.id.edit_text_message);
        String message = editTextMessage.getText().toString();

        // Set an intent object to load receiver activity
        Intent intent = new Intent(this, A102E_ReceiverActivity.class);

        // and put text message with the KEY into intent then start an activity.
        intent.putExtra("messageKey", message);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.example_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Close the current activity
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}


