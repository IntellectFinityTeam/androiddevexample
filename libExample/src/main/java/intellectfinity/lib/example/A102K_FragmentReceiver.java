package intellectfinity.lib.example;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

public class A102K_FragmentReceiver extends Fragment {
    LinearLayout receiverLayout;
    TextView messageText;

    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {

        // Set created layout to this fragment class.
        View view = inflater.inflate(
                R.layout.a102k_fragment_receiver,
                container,
                false);

        // Find the text view object
        messageText = (TextView) view.findViewById(R.id.text_message);

        return view;
    }

    // This method will be called from the class instant
    // And, update text view in the fragment layout base received input
    public void updateBackgroundColor(String colorName) {
        switch (colorName) {
            case "Red":
                messageText.setText("Red Selected");
                messageText.setTextColor(Color.RED);
                break;
            case "Green":
                messageText.setText("Green Selected");
                messageText.setTextColor(Color.GREEN);
                break;
            case "Blue":
                messageText.setText("Blue Selected");
                messageText.setTextColor(Color.BLUE);
                break;
            case "Gray":
                messageText.setText("Gray Selected");
                messageText.setTextColor(Color.GRAY);
                break;
            default:
                messageText.setText("No color selected.");
                break;
        }
    }
}

