package intellectfinity.lib.example;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class A102E_ReceiverActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Intent was used to open and pass text message to this activity
        // We you the same KEY to get text  message.
        Intent intent = getIntent();
        String message = intent.getStringExtra("messageKey");

        // Create text view and set text message
        TextView text_view = new TextView(this);
        text_view.setTextSize(25);
        text_view.setText("RECEIVED: \"" + message + "\"");

        // Set created text view to this class.
        // layout XML file does not require in this example.
        setContentView(text_view);

        // Set home should be displayed as an "up" affordance on ActionBar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.example_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Close the current activity
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

