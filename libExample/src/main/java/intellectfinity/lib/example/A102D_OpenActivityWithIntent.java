package intellectfinity.lib.example;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class A102D_OpenActivityWithIntent extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set created layout to this class.
        setContentView(R.layout.a102d_open_activity_with_intent);

        // Set home should be displayed as an "up" affordance on ActionBar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    // This method will be triggered by an event listener
    public void openActivity(View view) {
        // Set an intent object to load simple activity
        Intent intent = new Intent(this, A102D_SimpleActivity.class);

        // Then start activity with intent
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.example_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Close the current activity
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}

