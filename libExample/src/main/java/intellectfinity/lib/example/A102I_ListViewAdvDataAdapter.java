package intellectfinity.lib.example;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

// This class is custom array adapter which will be used by list view
public class A102I_ListViewAdvDataAdapter extends ArrayAdapter {

    List list = new ArrayList();

    static class DataHandler {
        ImageView emoImage;
        TextView emoDescription;
    }

    public A102I_ListViewAdvDataAdapter(Context context, int resource) {
        super(context, resource);
    }

    @Override
    public void add(Object object) {
        super.add(object);
        list.add(object);
    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int position) {
        return this.list.get(position);
    }

    // getView is the main part of your adapter. It triggers when you scroll the list view.
    // It returns View that will be displayed as your list view that use adapter item. .
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // When you scroll the view, the row goes out of the display area
        // of the mobile screen. As soon as the row goes out of the screen,
        // the Adapter View (ListView in this case)sends the view to something
        // called a recycler which is unused view and reuse it to display the new view.
        // convertView = unused view
        View row = convertView;
        DataHandler dataHandler;

        if (convertView == null) {
            // Inflate advanced layout to an unused view (row).
            LayoutInflater inflater =
                    (LayoutInflater) this
                            .getContext()
                            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(
                    R.layout.a102i_list_view_advanced_layout,
                    parent,
                    false);

            // Create data handler to hold the UI for the row
            // and will be assigned new row value in the next step
            dataHandler = new DataHandler();
            dataHandler.emoImage =
                    (ImageView) row.findViewById(R.id.image_emo);
            dataHandler.emoDescription =
                    (TextView) row.findViewById(R.id.text_description);

            // Set data handler to the row.
            row.setTag(dataHandler);
        } else {
            dataHandler = (DataHandler) row.getTag();
        }

        // Retrieve data of the new row to data provider and put it in the data handler
        A102I_ListViewAdvDataProvider dataProvider =
                (A102I_ListViewAdvDataProvider) this.getItem(position);
        dataHandler.emoImage.setImageResource(dataProvider.getEmoImageResource());
        dataHandler.emoDescription.setText(dataProvider.getEmoDescription());

        return row;
    }
}
