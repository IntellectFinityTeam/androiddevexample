package intellectfinity.lib.example;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class A102J_ActionBarAndMenu extends AppCompatActivity {

    // for Radio Context Menu
    int selectedRadioContextMenu = 0;

    // for Floating Context Menu
    Integer[] numbers = {100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200};
    ListView floatingMenuListView;
    ArrayAdapter<Integer> floatingMenuAdapter;
    List floatingMenuItems = new ArrayList<Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set created layout/view to this class.
        setContentView(R.layout.a102j_action_bar_and_menu);

        // Floating Context Menu ....................................
        // Create array adapter with build-in layout, string array resource
        for (Integer item : numbers) {
            floatingMenuItems.add(item);
        }
        floatingMenuAdapter =
                new ArrayAdapter<Integer>(
                        this,
                        android.R.layout.simple_list_item_1,
                        floatingMenuItems);

        // Find list view object and set adapter
        floatingMenuListView = (ListView) findViewById(R.id.list_floating_menu);
        floatingMenuListView.setAdapter(floatingMenuAdapter);

        //Registers a context menu to be shown for this list view
        registerForContextMenu(floatingMenuListView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu, this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.a102j_option_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here.
        int id = item.getItemId();
        String selectedMenu = "";
        LinearLayout menuLayout = (LinearLayout) findViewById(R.id.menu_layout);

        // Verify which menu was click
        // Unfortunately switch case does not support in the library project
        if (id == R.id.action_accept) {
            selectedMenu = "Accept";
        } else if (id == R.id.action_cancel) {
            selectedMenu = "Cancel";
        } else if (id == R.id.action_red) {
            selectedMenu = "RED";

            // Set background color to red
            menuLayout.setBackgroundColor(
                    getResources().
                            getColor(android.R.color.holo_red_light));
        } else if (id == R.id.action_green) {
            selectedMenu = "GREEN";

            // Set background color to green
            menuLayout.setBackgroundColor(
                    getResources().
                            getColor(android.R.color.holo_green_light));
        } else if (id == R.id.action_blue) {
            selectedMenu = "BLUE";

            // Set background color to blue
            menuLayout.setBackgroundColor(
                    getResources().
                            getColor(android.R.color.holo_blue_light));
        } else if (id == R.id.action_gray) {
            selectedMenu = "GRAY";

            // Set background color to gray
            menuLayout.setBackgroundColor(Color.parseColor("#EEEEEE"));
        } else {
            return super.onOptionsItemSelected(item);
        }

        // Show toast notification of selected menu
        Toast.makeText(
                getBaseContext(),
                selectedMenu + " was selected",
                Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public void onCreateContextMenu(
            ContextMenu menu,
            View v,
            ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if (v.getId() == R.id.button_radio_menu) {
            // Radio Context Menu .......................................
            // Inflate the menu when press a button.
            getMenuInflater().inflate(R.menu.a102j_radio_menu, menu);

            // Check the previous selection menu and enable it.
            switch (selectedRadioContextMenu) {
                case 1:
                    MenuItem menuDropbox =
                            menu.findItem(R.id.action_dropbox);
                    menuDropbox.setChecked(true);
                    break;
                case 2:
                    MenuItem menuGoogleDrive =
                            menu.findItem(R.id.action_google_drive);
                    menuGoogleDrive.setChecked(true);
                    break;
                case 3:
                    MenuItem menuOneDrive =
                            menu.findItem(R.id.action_one_drive);
                    menuOneDrive.setChecked(true);
                    break;
            }

        } else if (v.getId() == R.id.list_floating_menu) {
            // Floating Context Menu .....................................
            // Inflate the menu when press and hold the list item.
            getMenuInflater().inflate(R.menu.a102j_floating_menu, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        int id = item.getItemId();
        String selectedMenu = "";

        if (id == R.id.action_dropbox ||
                id == R.id.action_google_drive ||
                id == R.id.action_one_drive) {
            // Radio Context Menu ........................................
            // Handle radio menu item clicks here.
            if (id == R.id.action_dropbox) {
                selectedMenu = "Dropbox";
                selectedRadioContextMenu = 1;
            } else if (id == R.id.action_google_drive) {
                selectedMenu = "Google Drive";
                selectedRadioContextMenu = 2;
            } else if (id == R.id.action_one_drive) {
                selectedMenu = "One Drive";
                selectedRadioContextMenu = 3;
            } else {
                return super.onContextItemSelected(item);
            }

            // Show toast notification of selected menu
            Toast.makeText(
                    getBaseContext(),
                    selectedMenu + " selected as a cloud drive.",
                    Toast.LENGTH_SHORT).show();

        } else if (id == R.id.action_delete ||
                id == R.id.action_share ||
                id == R.id.action_help) {
            // Floating Context Menu .....................................
            // Handle radio menu item clicks here.
            AdapterView.AdapterContextMenuInfo info =
                    (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
            if (id == R.id.action_delete) {
                // Remove item from list view
                floatingMenuItems.remove(info.position);
                floatingMenuAdapter.notifyDataSetChanged();
                selectedMenu = "Delete";
            } else if (id == R.id.action_share) {
                selectedMenu = "Share";
            } else if (id == R.id.action_help) {
                selectedMenu = "Help";
            } else {
                return super.onContextItemSelected(item);
            }

            // Show toast notification of selected menu
            Toast.makeText(
                    getBaseContext(),
                    selectedMenu + " selected at item=" +
                            ((TextView) info.targetView).getText().toString(),
                    Toast.LENGTH_SHORT).show();
        }

        return true;
    }

    // Popup context menu on button click ....................
    public void showPopupContextMenu(View view) {
        // Inflate the menu when press a button.
        PopupMenu popupMenu = new PopupMenu(this, view);
        MenuInflater menuInflater = popupMenu.getMenuInflater();
        menuInflater.inflate(R.menu.a102j_popup_menu, popupMenu.getMenu());

        // Set the event handler
        PopupMenuEventHandle popupMenuEventHandle =
                new PopupMenuEventHandle(getApplicationContext());
        popupMenu.setOnMenuItemClickListener(popupMenuEventHandle);
        popupMenu.show();
    }

    // Event handler class to handle popup menu item click
    public class PopupMenuEventHandle implements PopupMenu.OnMenuItemClickListener {
        Context context;

        public PopupMenuEventHandle(Context context) {
            this.context = context;
        }

        // This method will be triggered when popup menu item is clicked
        @Override
        public boolean onMenuItemClick(MenuItem item) {

            String selectedAccount = "";

            if (item.getItemId() == R.id.action_facebook) {
                selectedAccount = "Facebook";
            } else if (item.getItemId() == R.id.action_google_plus) {
                selectedAccount = "Google+";
            } else if (item.getItemId() == R.id.action_microsoft) {
                selectedAccount = "Microsoft";
            } else {
                return false;
            }

            // Show toast notification of selected menu
            Toast.makeText(
                    context,
                    "Login with " + selectedAccount + " account.",
                    Toast.LENGTH_SHORT).show();
            return true;
        }
    }

    // Radio context menu on button click ....................
    public void showRadioContextMenu(View view) {
        //Registers a context menu to be shown for this button
        registerForContextMenu(view);

        // Open context menu will call onCreateContextMenu method
        openContextMenu(view);
    }

}

