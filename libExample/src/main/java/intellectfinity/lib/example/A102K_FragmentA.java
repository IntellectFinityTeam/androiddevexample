package intellectfinity.lib.example;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class A102K_FragmentA extends Fragment {
    @Nullable
    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {

        // Set created layout to this fragment class.
        View view = inflater.inflate(
                R.layout.a102k_fragment_a,
                container,
                false);

        return view;
    }
}
