package intellectfinity.lib.example;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

public class A102K_FragmentSender extends Fragment {
    RadioGroup colorRadioGroup;
    String selectedColor = "";
    Button updateButton;

    // For Fragment to Main Activity ..........................................
    // - Sender fragment will pass message/data to main activity and
    // - Main activity will process the message/data
    OnColorChangedListener onColorChangedListener;

    // Main activity must implement this interface to communicate with fragment
    public interface OnColorChangedListener {
        public void colorChanged(String colorName);
    }

    // For Fragment to another Fragment .......................................
    // - Sender fragment will pass message/data to main activity and
    // - Main activity will forward message/data to receiver fragment
    // - aka. Fragment (to main activity then) to another Fragment
    OnButtonClickedListener onButtonClickedListener;

    // Main activity must implement this interface to communicate with fragment
    public interface OnButtonClickedListener {
        public void buttonClicked(String colorName);
    }

    @Override
    // onAttach method is a very first method thai will be called.
    // Once the fragment attach to activity then set fragment interface with an activity
    // The communication channel(interface) is set at this step.
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // For Fragment to Main Activity
            onColorChangedListener = (OnColorChangedListener) activity;

            // For Fragment to another Fragment
            onButtonClickedListener = (OnButtonClickedListener) activity;
        } catch (Exception e) {

        }
    }

    @Nullable
    @Override
    // onCreateView method will be called after onAttach.
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {

        // Set created layout to this fragment class.
        View view = inflater.inflate(
                R.layout.a102k_fragment_sender,
                container,
                false);

        // Fragment to Main Activity .......................................
        colorRadioGroup = (RadioGroup) view.findViewById(R.id.rg_color);
        colorRadioGroup.setOnCheckedChangeListener(
                new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    // The interface(onColorChangedListener) was set in onAttach method
                    // The fragment uses this interface to communicate main activity
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if (checkedId == R.id.rb_red) {
                            onColorChangedListener.colorChanged("Red");
                            selectedColor = "Red";
                        } else if (checkedId == R.id.rb_green) {
                            onColorChangedListener.colorChanged("Green");
                            selectedColor = "Green";
                        } else if (checkedId == R.id.rb_blue) {
                            onColorChangedListener.colorChanged("Blue");
                            selectedColor = "Blue";
                        } else if (checkedId == R.id.rb_gray) {
                            onColorChangedListener.colorChanged("Gray");
                            selectedColor = "Gray";

                        }
                    }
                });

        // For Fragment to another Fragment .......................................
        updateButton = (Button) view.findViewById(R.id.button_update);
        updateButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    // The interface(onButtonClickedListener) was set in onAttach method
                    // The fragment uses this interface to communicate main activity
                    public void onClick(View v) {
                        onButtonClickedListener.buttonClicked(selectedColor);
                    }
                });

        return view;
    }
}