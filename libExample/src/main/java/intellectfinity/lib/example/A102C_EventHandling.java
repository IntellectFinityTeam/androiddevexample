package intellectfinity.lib.example;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

public class A102C_EventHandling extends AppCompatActivity {

    TextView textMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set created layout/view to this class.
        setContentView(R.layout.a102c_event_handling);

        // Find the text view object
        textMessage = (TextView) findViewById(R.id.text_message);

        // Set home should be displayed as an "up" affordance on ActionBar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    // This method will be triggered by an event listener
    public void updateText(View view) {
        String greetingText = "Welcome to EventHandling\n(o_O)";
        String helloText = "Hello from EventHandling\n(O_o)";

        // Get/Set text view
        if (textMessage.getText().toString().equals(helloText)) {
            textMessage.setText(greetingText);
        } else {
            textMessage.setText(helloText);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.example_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Close the current activity
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
