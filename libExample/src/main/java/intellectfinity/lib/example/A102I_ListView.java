package intellectfinity.lib.example;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class A102I_ListView extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Set created layout/view to this class.
        setContentView(R.layout.a102i_list_view);

        // ListView - Simple Layout ............................................
        // Create array adapter with build-in layout, string array resource
        // Try it yourself layout = simple_list_item_1 or simple_list_item_multiple_choice
        ArrayAdapter<String> simpleListViewAdapter =
                new ArrayAdapter<String>(
                        this, // Context
                        android.R.layout.simple_list_item_multiple_choice, // Layout
                        getResources().getStringArray(R.array.day_arrays)); // Data

        // Find list view object and set adapter, choice mode
        ListView simpleListView =
                (ListView) findViewById(R.id.list_view_simple);
        simpleListView.setAdapter(simpleListViewAdapter);
        simpleListView.setChoiceMode(
                ListView.CHOICE_MODE_SINGLE); // CHOICE_MODE_MULTIPLE

        // This method will be triggered by a list view event listener
        simpleListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent,
                                            View view,
                                            int position,
                                            long id) {
                        Toast.makeText(
                                getBaseContext(),
                                parent.getItemAtPosition(position) +
                                        " in simple layout was selected.",
                                Toast.LENGTH_SHORT).show();
                    }
                }
        );

        // ListView - Custom Layout .............................................
        // Create array adapter with custom layout
        // And refer to text view in the custom layout
        ArrayAdapter<String> customListViewAdapter =
                new ArrayAdapter<String>(
                        this, // Context
                        R.layout.a102i_list_view_custom_layout, //Custom Layout
                        R.id.list_view_custom_item, // TextView
                        getResources().getStringArray(R.array.month_arrays)); // Data

        // Find list view object and set adapter
        ListView customListView = (ListView) findViewById(R.id.list_view_custom);
        customListView.setAdapter(customListViewAdapter);

        // This method will be triggered by a list view event listener
        customListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent,
                                            View view,
                                            int position,
                                            long id) {
                        Toast.makeText(
                                getBaseContext(),
                                parent.getItemAtPosition(position) +
                                        " in custom layout was selected.",
                                Toast.LENGTH_SHORT).show();

                        // Updated row/item state which will reflect it's background color.
                        // Because of the layout use a selector to control background color.
                        view.setSelected(true);
                    }
                }
        );

        // ListView - Advanced Layout ..........................................
        int[] emoImageResource = {
                R.drawable.ic_action_emo_angry,
                R.drawable.ic_action_emo_basic,
                R.drawable.ic_action_emo_cool,
                R.drawable.ic_action_emo_cry,
                R.drawable.ic_action_emo_err,
                R.drawable.ic_action_emo_evil,
                R.drawable.ic_action_emo_kiss,
                R.drawable.ic_action_emo_laugh,
                R.drawable.ic_action_emo_shame,
                R.drawable.ic_action_emo_tongue,
                R.drawable.ic_action_emo_wink,
                R.drawable.ic_action_emo_wonder
        };

        final String[] emoDescription = {
                "Angry",
                "Basic",
                "Cool",
                "Cry",
                "Err",
                "Evil",
                "Kiss",
                "Laugh",
                "Shame",
                "Tongue",
                "Wink",
                "Wonder"
        };

        // Initiate dataAdapter with created advanced layout
        A102I_ListViewAdvDataAdapter dataAdapter =
                new A102I_ListViewAdvDataAdapter(getApplicationContext(),
                        R.layout.a102i_list_view_advanced_layout);

        // Find list view object and set adapter
        ListView advancedListView = (ListView) findViewById(R.id.list_view_advanced);
        advancedListView.setAdapter(dataAdapter);

        // Update data in the adapter
        // For each item, create new dataProvider object then, add into dataAdapter
        for (int i = 0; i < emoImageResource.length; i++) {
            A102I_ListViewAdvDataProvider dataProvider =
                    new A102I_ListViewAdvDataProvider(
                            emoImageResource[i],
                            emoDescription[i]);
            dataAdapter.add(dataProvider);
        }

        // This method will be triggered by a list view event listener
        advancedListView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent,
                                            View view,
                                            int position,
                                            long id) {
                        Toast.makeText(
                                getBaseContext(),
                                emoDescription[position] + " in adv layout was selected.",
                                Toast.LENGTH_SHORT).show();
                    }
                }
        );

        // Set home should be displayed as an "up" affordance on ActionBar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.example_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Close the current activity
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
